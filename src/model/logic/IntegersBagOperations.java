package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations
{		
	public double computeMean(IntegersBag bag)
	{
		double mean = 0;
		int length = 0;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				mean += iter.next();
				length++;
			}
			if( length > 0)
				mean = mean / length;
		}
		return mean;
	}
		
	public int getMax(IntegersBag bag)
	{
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if( max < value)
					max = value;
			}
		}
		return max;
	}
	
	//Retorna el valor minimo del arreglo
	public int getMin (IntegersBag bag)
	{
		int minimo = Integer.MAX_VALUE;
		int siguiente; 
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				siguiente = iter.next();
				if( minimo > siguiente)
					minimo = siguiente; 
			}
		}
		return minimo; 		
	}
	
	//Retorna los numeros pares. 
	public ArrayList<Object> darPares (IntegersBag bag)
	{
		ArrayList<Object> retorno = new ArrayList<>(); 
		if(bag != null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				int siguiente = iter.next();
				if(siguiente%2 == 0 )
					retorno.add(siguiente); 
			}
		}
		return retorno;
	}
	
	//Retorna los numeros impares. 
		public ArrayList<Object> darImpares (IntegersBag bag)
		{
			ArrayList<Object> retorno = new ArrayList<>(); 
			if(bag != null)
			{
				Iterator<Integer> iter = bag.getIterator();
				while(iter.hasNext())
				{
					int siguiente = iter.next();
					if(siguiente%2 != 0 ) 
						retorno.add(siguiente); 
				}
			}
			return retorno;
		}
}
