package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IntegersBag;

public class MVCExample {
	
	
	
	
	private static void printMenu(){
		System.out.println("1. Create a new bag of integers (i.e., a set of unique integers)");
		System.out.println("2. Compute the mean");
		System.out.println("3. Get the max value");
		
		//Visibilidad de las 3 operaciones ------------------------------------------------
		System.out.println("4. Obtener el valor minimo");
		System.out.println("5. Obtener los numeros pares");
		System.out.println("6. Obtener los numeros impares");
		//---------------------------------------------------------------------------------
		
		System.out.println("7. Exit");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
	
	private static ArrayList<Integer> readData(){
		ArrayList<Integer> data = new ArrayList<Integer>();
		
		System.out.println("Please type the numbers (separated by spaces), then press enter: ");
		try{
			
			String line = new Scanner(System.in).nextLine();
		
			String[] values = line.split("\\s+");
			for(String value: values){
				data.add(new Integer(value.trim()));
			}
		}catch(Exception ex){
			System.out.println("Please type integer numbers separated by spaces");
		}
		return data;
	}

	public static void main(String[] args){
		
		IntegersBag bag = null;
		Scanner sc = new Scanner(System.in);
		
		for(;;){
		  printMenu();
		  
		 
		  int option = sc.nextInt();
		  switch(option){
			  case 1: bag = Controller.createBag(readData()); System.out.println("--------- \n The bag was created  \n---------");
			  break;
			  
			  case 2: System.out.println("--------- \n The mean value is "+Controller.getMean(bag)+" \n---------");
			  break;
			  
			  case 3: System.out.println("--------- \nThe max value is "+Controller.getMax(bag)+" \n---------");		  
			  break;
			  
			  //Retorno del valor minimo------------------------------------------------------------------------------------------------
			  case 4: System.out.println("--------- \nEl valor minimo es "+Controller.darMinimo(bag)+" \n---------");		  
			  break;			  
			  //Retorna un arreglo con los numeros pares
			  case 5: System.out.println("--------- \nLos numeros pares son "+Controller.darPares(bag)+" \n---------");		  
			  break;
			  //Retorna un arreglo con los numeros impares
			  case 6: System.out.println("--------- \nLos numeros impares son "+Controller.darImpares(bag)+" \n---------");		  
			  break;
			  //---------------------------------------------------------------------------------------------------------------------------
			  
			  case 7: System.out.println("Bye!!  \n---------"); sc.close(); return;		  
			  default: System.out.println("--------- \n Invalid option !! \n---------");
		  }
		}
	}
	
}
