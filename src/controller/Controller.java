package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(IntegersBag bag){
		return model.getMax(bag);
	}

	//Retorna el minimo
	public static int darMinimo(IntegersBag bag)
	{
		return model.getMin(bag); 
	}

	//Retorna los valores pares
	public static ArrayList<Object> darPares(IntegersBag bag)
	{
		return model.darPares(bag); 
	}

	//Retorna los valores impares
	public static ArrayList<Object> darImpares(IntegersBag bag)
	{
		return model.darImpares(bag); 
	}

}
